FROM ruby:2.5

WORKDIR /alpha_api

COPY Gemfile Gemfile.lock ./

RUN bundle install

COPY . .

LABEL maintainer="Purkhalo Alexander: https://t.me/alexpurhalo"

CMD ["bundle", "exec", "rake", "-T"]
