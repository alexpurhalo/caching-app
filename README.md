### Test Assignment
Подготовить скелет REST API с механизмом для управления клиентским кешем:
 - клиент должен иметь возможность закешировать данные переданные с сервера любым GET запросом c помощью Etag
  - клиент должен иметь возможность с помощью HEAD запроса проверить актуальность закешированных данных
  - сервер не должен хранить у себя информацию о том какие данные были переданы клиенту

Требование к решению:
  - универсальность и возможность применить к любому API endpoint которые обрабатывает GET
  - покрытие тестами не меньше 80%
*  *  *  *  *

### Usage via Docker, version: 18.06.1-ce
```
$ docker image build --tag caching-app-image  .
$ docker run -it --rm -p 1234:1234 caching-app-image rake db:migrate && rake db:seed && rake start
$ docker run -it --rm --env RACK_ENV=test caching-app-image rake db:migrate && rspec
```

### Guide
```
$ curl -X GET http://localhost:1234/items
$ curl -X POST -d "{\"name\":\"icecream\",\"price\":82}" -H 'Content-type:application/json' http://localhost:1234/items
$ curl -X GET http://localhost:1234/items/1
$ curl -X PUT -d "{\"price\":41}" -H "Content-type:application/json" http://localhost:1234/items/1
$ curl -X DELETE http://localhost:1234/items/1
```

### Try Caching
```
curl -X GET -i http://localhost:1234/items
curl -X GET -i -H "If-None-Match:fc9833f882463f0f71e55a9703289fd0" http://localhost:1234/items
```
*  *  *  *  *

### Usage in Dev environment
```
$ bundle install
$ rake db:migrate
$ rake db:seed
$ rake start
```

### Usage in Test environment
```
$ bundle install
$ RACK_ENV=test rake db:migrate
$ rspec
$ google-chrome coverage/index.html
```
