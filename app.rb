require 'rubygems'
require 'bundler/setup'

Bundler.require :default, (ENV['RACK_ENV'] || 'development').to_sym

DB = Sequel.connect("sqlite://db/#{ENV['RACK_ENV'] || 'development'}.db")

class App < Grape::API
  format :json

  get '/items/' do
    DB[:items].all
  end

  params do
    requires :name,  type: String
    requires :price, type: Integer
  end
  post '/items/' do
    DB[:items].insert(params)
    DB[:items].all
  end

  get '/items/:id' do
    DB[:items][id: params[:id]]
  end

  params do
    optional :name, type: String
    optional :price, type: Integer
    at_least_one_of :name, :price
  end
  put '/items/:id' do
    DB[:items][id: params[:id]].update(params)
  end

  delete '/items/:id' do
    DB[:items].where(id: params[:id]).delete
    DB[:items].all
  end
end
