require './app'
require './lib/caching_middleware'

use CachingMiddleware
run App
