Sequel.migration do
  up do
    create_table :items do
      primary_key :id

      String :name,   null: false
      Integer :price, null: false
    end
  end

  down do
    drop_table :items
  end
end
