require 'digest'

class CachingMiddleware
  def initialize(app)
    @app = app
  end
  
  def call(env)
    status, headers, response = @app.call(env)

    return [status, headers, response] unless env['REQUEST_METHOD'] === 'GET'
    return [status, headers, response] unless status === 200

    etag = Digest::MD5.hexdigest(response.body.to_s)

    return [304, {'Etag' => etag}, []] if etag === env['HTTP_IF_NONE_MATCH']

    [status, headers.merge('Etag' => etag), response]
  end
end

