require_relative './spec_helper'

describe 'GET /items' do
  before {
    DB[:items].insert(:name => 'apple',  :price => 32)
    DB[:items].insert(:name => 'orange', :price => 64)
  }

  before { get '/items/' }

  it 'returns items' do
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body)).to have_exactly(2).items
    expect(JSON.parse(last_response.body)).to include(hash_including('name', 'price'),)
  end
end

describe 'POST /items' do
  before {
    DB[:items].insert(:name => 'apple',  :price => 32)
    DB[:items].insert(:name => 'orange', :price => 64)
  }

  before { post '/items/', { name: 'grape', price: 200} }

  it 'returns list of updated posts' do
    expect(last_response.status).to eq(201)
    expect(JSON.parse(last_response.body)).to have_exactly(3).items
    expect(JSON.parse(last_response.body)).to include(hash_including('name', 'price'))
  end
end

describe 'GET /items/:id' do
  before { DB[:items].insert(:name => 'apple',  :price => 32) }
  before { get '/items/1' }

  it 'returns item' do
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body)).to include('name', 'price')
  end
end

describe 'PUT /items/:id' do
  before { DB[:items].insert(:name => 'apple',  :price => 32) }
  before { put '/items/1', { name: 'grape' } }

  it 'returns updated item' do
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body))
        .to include('name' =>'grape', 'price' => 32)
  end
end

describe 'DELETE /items/:id' do
  before {
    DB[:items].insert(:name => 'apple',  :price => 32)
    DB[:items].insert(:name => 'orange', :price => 64)
  }

  before { delete '/items/1' }

  it 'returns updated items' do
    expect(last_response.status).to eq(200)
    expect(JSON.parse(last_response.body))
        .not_to include(hash_including('name' => 'apple', 'price' => 32))
  end
end