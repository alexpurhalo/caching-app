require_relative '../spec_helper'
require 'digest'

describe CachingMiddleware do
  context 'when server returns a GET 200' do
    let(:mocked_resp) { Rack::MockResponse.new(200, {'Content-Type' => 'application/json'}, [["OK"]]) }
    let(:app) { lambda { |env| [mocked_resp.status, mocked_resp.headers, mocked_resp]} }
    let(:middleware) { CachingMiddleware.new(app) }
    let(:request) { Rack::MockRequest.new(middleware) }

    context 'with matching "If-none-match" and "Etag"' do
      let(:etag) { Digest::MD5.hexdigest(mocked_resp.body.to_s) }
      subject(:response) { request.get('/', {'HTTP_IF_NONE_MATCH' => etag }) }

      it 'returns the 304 status'  do
        expect(response.status).to eq 304
      end

      it 'returns an empty body' do
        expect(response.body).to be_empty
      end
    end

    context 'but "If-none-match" and "Etag" do not match' do
      subject(:response) { request.get('/', {'HTTP_IF_NONE_MATCH' => "" }) }

      it 'returns the original status' do
        expect(response.status).to eq(mocked_resp.status)
      end

      it 'returns the original content' do
        expect(response.body).to eq(mocked_resp.body)
      end

      it 'return headers with an attached "Etag"' do
        expect(response.header['Etag']).not_to be_empty
      end
    end
  end

  context 'when server returns NOT a GET 200' do
    let(:mocked_resp) { Rack::MockResponse.new(201, {'Content-Type' => 'application/json'}, [["OK"]]) }
    let(:app) { lambda { |env| [mocked_resp.status, mocked_resp.headers, mocked_resp]} }
    let(:middleware) { CachingMiddleware.new(app) }
    let(:request) { Rack::MockRequest.new(middleware) }
    subject(:response) { request.post('/') }

    it 'returns original response' do
      expect(response.status).to eq(mocked_resp.status)
      expect(response.body).to eq(mocked_resp.body)
      expect(mocked_resp.header).to eq(response.header)
    end
  end
end
